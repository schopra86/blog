class Post < ActiveRecord::Base
  belongs_to :users
  has_many :comments, dependent: :destroy
  validates_presence_of :title
  validates_presence_of :body
end
